let custom = {
    /**
     * Add your custom pairs here.
     * One per line, with quotation marks around. Comma at end of line.
     * 
     * E.g. "US30",
     */
    customPairs: [
      
    ],

    /**
     * Add your custom setups here.
     * One per line, with quotation marks around. Comma at end of line.
     * 
     * E.g. "Cheese man",
     */
    customSetups: [

    ],
}